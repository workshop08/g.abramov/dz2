package Package1;

public class Archer extends Unit {

    public Archer(String type, String civ, int armor, int damage, int moves) {
        super(type, civ, armor, damage, moves);
    }

    public void equipWeapon(Weapon item) {
        if (item.getType() == "Bow") {
            this.EquipedWeapon = item.type;
            this.damage += item.getDamage();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

    public void equipArmor(Armor item) {
        if (item.getType() == "Helmet") {
            this.EquipedArmor = item.type;
            this.armor += item.getDefense();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

}