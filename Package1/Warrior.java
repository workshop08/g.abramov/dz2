package Package1;

public class Warrior extends Unit {

    public Warrior(String type, String civ, int armor, int damage, int moves) {
        super(type, civ, armor, damage, moves);
    }

    public void equipWeapon(Weapon item) {
        if ((item.getType() == "Spear") || (item.getType() == "Sword")) {
            this.EquipedWeapon = item.type;
            this.damage += item.getDamage();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

    public void equipArmor(Armor item) {

        if (item.getType() == "Shield") {
            this.EquipedArmor = item.type;
            this.armor += item.getDefense();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

}