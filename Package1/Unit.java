//TODO: Добавить Item как поле Unit
//TODO: Добавить в детей Unit ещё методов и полей
//TODO: Нейминг функций (сегодня расскажем)
//TODO: Сделать строки полями-константами и через enum
//TODO: Переименовать Package1
//TODO: Сделать println в несколько строчек по характеристике на каждую строчку
package Package1;

import com.sun.source.tree.IfTree;

public class Unit {
    protected int armor, damage, moves, id;
    protected String civ, type, EquipedWeapon, EquipedArmor;
    protected static int i = 0;

    protected Unit(String type, String civ, int armor, int damage, int moves) {
        this.armor = armor;
        this.type = type;
        this.EquipedArmor = "false";
        this.EquipedWeapon = "false";
        this.damage = damage;
        this.moves = moves;
        this.civ = civ;
        this.id = i;
        i++;
    }

    public void Print() {
        System.out.println("This unit is " + this.civ + "'s " + this.type + ".\nArmor - " + this.armor
                + ".\nDamage - " + this.damage + ".\nMoves - " + this.moves + ".");
        if (this.EquipedWeapon != "false") {
            System.out.println("Weapon - " + this.EquipedWeapon + ".");
        }
        if (this.EquipedArmor != "false") {
            System.out.println("Armor - " + this.EquipedArmor + ".");
        }
        System.out.println("ID - " + this.id + ".\n");
    }

    public int getId() {
        return this.id;
    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int a) {
        this.damage = a;
    }

    public int getMoves() {
        return this.moves;
    }

    public void setMoves(int a) {
        this.moves = a;
    }

    public String getCiv() {
        return this.civ;
    }

    public void setCiv(String a) {
        this.civ = a;
    }

    public int getArmor() {
        return this.armor;
    }

    public void setArmor(int a) {
        this.armor = a;
    }

    public String getEquipedArmor() {
        return this.EquipedArmor;
    }

    public String getEquipedWeapon() {
        return this.EquipedWeapon;
    }

}