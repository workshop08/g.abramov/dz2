package Package1;

public class Item {
    protected int status, id;
    protected String type;
    protected static int i = 0;

    protected Item(String type, int status) {
        this.type = type;
        this.status = status;
        this.id = i;
        i++;
    }

    public void Print() {
        System.out.println("This is " + this.type + ".\nStatus - " + this.status + ".\nID - " + this.id + ".");
    }

    public int getId() {
        return this.id;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int a) {
        this.status = a;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String a) {
        this.type = a;
    }
}