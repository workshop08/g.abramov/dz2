package Package1;

public class Armor extends Item{
    private int defense;

    public Armor(String type, int status, int damage){
        super(type, status);
        this.defense = damage;
    }

    public void Print() {
        super.Print();
        System.out.println("Defense - " + this.defense + ".\n");
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int a) {
        this.defense = a;
    }

}